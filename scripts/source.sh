: ${ASK="y"}
: ${QUIET="n"}
: ${PRETEND="n"}
: ${VERBOSE="y"}

if [[ ${QUIET} == [yY1] ]]
then
	[[ ${ASK} == [yY1] ]] && {
		echo "${argv0}: incompatible options: QUIET=y && ASK=y!" 1>&2
		exit 1
	}

	[[ ${PRETEND} == [yY1] ]] && {
		echo "${argv0}: incompatible options: QUIET=y && PRETEND=y!" 1>&2
		exit 1
	}

	[[ ${VERBOSE} == [yY1] ]] && {
		echo "${argv0}: incompatible options: QUIET=y && VERBOSE=y!" 1>&2
		exit 1
	}
fi

msg() {
	[[ ${QUIET} == [nN0] ]] && echo "$@"
}

indent() {
	[[ ${VERBOSE} == [yY1] ]] && echo
}

die() {
	(( $# > 0 )) && msg "$@"
	exit 1
}

ask_cmd() {
	local confirm

	if [[ ${VERBOSE} == [yY1] ]]
	then
		msg "$1:"
		msg "${*:2}"

		if [[ ${ASK} == [yY1] ]]
		then
			printf "Press [Y/N/S]: "
		fi
	else
		if [[ ${ASK} == [yY1] ]]
		then
			printf "%s: [Y/N/S]: " "$1"
		else
			msg "$1"
		fi
	fi

	if [[ ${ASK} == [yY1] ]]
	then
		while read confirm
		do
			[[ ${confirm} == [yY] ]] && break
			[[ ${confirm} == [nN] ]] && exit
			[[ ${confirm} == [sS] ]] || continue
			indent
			return
		done
	fi

	if [[ ${PRETEND} == [nN0] ]]
	then
		"${@:2}" >/dev/null || die "$1 failed!"
	fi

	indent
}

run_cmd() {
	msg "$*"

	if [[ ${PRETEND} == [nN0] ]]
	then
		"$@" >/dev/null || die "$1 failed!"
	fi
}

sleep1() {
	local -i seconds=$1
	while (( seconds > 0 ))
	do
		msg "$2: ${seconds} second(s) remain"

		[[ ${PRETEND} == [nN0] ]] && sleep 1

		(( seconds -= 1 ))
	done

	indent
}

dev_ck() {
	[[ -b "$1" || -f "$1" ]] || die "$1 does not exist!"
	mount | grep -q "^$1" && die "Partition(s) on $1 are already mounted!"
}

dev_info() {
	local dev_path="$1"
	local dev_size=$(lsblk -dn -o SIZE "${dev_path}")

	msg "Using device: ${dev_path}"
	msg "Device size: ${dev_size}"
}

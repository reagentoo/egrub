# Egrub

Egrub is a simple GRUB wrappers and automation scripts. The main idea -
automatic boot menu using grub-shell. So you would not need to run
`grub-mkconfig` every time after new kernel installation.
`egrub mkconfig` can be run only once.

Egrub is adapted for use with the Gentoo dist-kernel and genkernel:

```sh
USE="-initramfs" emerge gentoo-kernel
genkernel ramdisk
```

It also supports loading Gentoo livecd iso from the local Gentoo mirror.

## Basic usage

```sh
git clone https://gitlab.com/reagentoo/egrub.git
cd egrub
cp -r emod /boot/grub
nano egrub.d/50_gentoo
EGRUB_D=egrub.d scripts/egrub mkconfig /boot/grub/grub.cfg
```

## Install from overlay

```sh
eselect repository add reagentoo git https://gitlab.com/reagentoo/gentoo-overlay.git
emerge egrub
cp -r /usr/lib/grub/emod /boot/grub
nano /etc/egrub.d/50_gentoo
egrub mkconfig /boot/grub/grub.cfg
```

## Using with encrypted EDISK and EFLASH usb stick

```sh
nano /etc/egrub.d/30_eprobe

PRETEND=1 esync partial /mnt/mirror/gentoo
PRETEND=0 esync partial /mnt/mirror/gentoo

PRETEND=1 eflash create /dev/sdx
PRETEND=0 eflash create /dev/sdx

PRETEND=1 VG_NAME=vg1 edisk create /dev/sdz
PRETEND=0 VG_NAME=vg1 edisk create /dev/sdz
```

## Screenshots

### Title
![image](https://gitlab.com/reagentoo/egrub/-/wikis/uploads/60d58d23d2840902dd962344b9d2a3aa/image.png)

### Kernels

![image](https://gitlab.com/reagentoo/egrub/-/wikis/uploads/ff5ba3c3136eee62056b8158973c792d/image.png)

### LiveCDs

![image](https://gitlab.com/reagentoo/egrub/-/wikis/uploads/3de4c82b6d4579256fc9362268137787/image.png)

## License

MIT
